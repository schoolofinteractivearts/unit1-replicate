﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerScript : MonoBehaviour {
	// Assign the GameObject to explode
	public GameObject bomb;

	// int means Integer (whole numbers). hp stands for hit points. We use this variable to keep track of how many times we press Space
	public int hp;

	// GameObject is a class name, describing the explosionPrefab object. Prefab is the template we use to instantiate(make copy of)
	public GameObject explosionPrefab;


	// Update is called once per frame
	void Update()
	{
		// If statement: anything between the curly brackets {} will only happen if the game met the condition
		// Here, the condition is Input.GetKeyDown(KeyCode.Space), which means that the key got pressed down
		if (Input.GetKeyDown(KeyCode.Space))
		{
			// We make hp decrease by 1. Remember to initialize it on the inspector
			hp -= 1;


			// If statement: anything between the curly brackets {} will only happen if the game met the condition
			if (hp == 0)
			{
				// Instantiate (make copy of) the template in the scene
				Instantiate(explosionPrefab);

				// GetComponent is used to get the component for given game object. In this case, the AudioSource is on GameManager itself
				// After we got the component, we call the function named Play() from it
				GetComponent<AudioSource>().Play();

				// Destroy the bomb game object
				// Destroy is a function / method, it requires an argument, meaning the thing the function wants to destroy. In this case, the bomb
				Destroy(bomb);

				// Use Debug.Log to show string(sentences)
				Debug.Log("Destroyed!");
			}
		}
	}
}
